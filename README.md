﻿JavaMachineFactory 
======================
author Tomáš Sýkora (steache@gmail.com)  
since 2014-03-28  
version: 0.4.4

#Changelog

2015-06-10 (0.4.4)
  - další řada refactoru

2015-06-09
  - refactor, reformat a ještě budu pokračovat

2014-05-02  
  -  zhruba návrh optimalizace  
  -  procházím jednotlivé priority, takže např. projdu vše pro blocker a zjistím, že jen některé stroje mají tento typ jako blocker(nejvyšší prioritu pro výrobu) a hned je zapnu a zapíšu si, kolik jsem získal  
  -  až dojdu na konec, tak se stane to, že jsem šel od nejvyšší priority po nejnižší, tkaže jsem se nejdřív pokusil zapnout stroje pro typ součástky, které tuto součástku mají jako nejvyšší prioritu pro výrobu...   
  -  pokud se dostanu na konec, tak vidím, co jsem nastavil a co né ...  
  -  stroje, co jsou aktuálně stále off asi nemohou vyrábět součástky, co potřebuji, nebo nemají dostatečnou kapacitu  
  -  zkusím, jestli ji může vyrábět, pokud né, tak jej označím k odebrání z továrny  
  -  pokud jen nemá dostatečnou kapacitu, tak ho nechceme odstranit, ale pouze přidat další  
  -  celý je to hrozně škaredý, ale je to jen návrh funkce ...  

2014-04-26  
  -  odstranění metody a celé logiky okolo counts, protože je to již zbytečné ... dá se jednoduše spočítat počet strojů v továrně pomocí f.getMachines().get(mtype).size() :)  

2014-04-25  
  -  Spousta drobností, nějaké statistiky  
  -  g)	Výpis počtu strojů v jednotlivých kategoriích a jejich aktuálně volné kapacity  
  -  h)	Výpis celkové aktuální energetické náročnosti všech strojů (jedno číslo ve W/h)  
  -  f)	Oprava stroje - uživatel zadá identifikační číslo porouchaného stroje, (bez řešení optimalizace)  
  -  e)	Porucha stroje – uživatel zadá identifikační číslo stroje, který se porouchal.  (bez řešení optimalizace)
  -  d)	Odstranění stroje z továrny – uživatel zadá identifikační číslo stroje, který chce odstranit a ten bude z databáze smazán, (bez řešení optimalizace)  
  -  a)	Přidávat nové stroje do továrny - uživatel si vždy pouze vybere typ stroje, který chce přidat a zadá jeho identifikační číslo.  
  
2014-04-22   
  -  Abstract machine factory  
  -  Ošetřený vstup  
  
2014-03-29  
  -  Rozšíření návrhu tříd  
  -  Přidání globálního textového "api"  
  -  Změna řešení uložení strojů v poli Machines ze stroje na ArrayList strojů, kvůli id, nejdřív jsem si neuvědomil, že by tam ten count byl k ničemu...   
  -  Náznak přidání nového stroje, pár drobných změn
 
2014-03-28  
  - Základní návrh tříd  

