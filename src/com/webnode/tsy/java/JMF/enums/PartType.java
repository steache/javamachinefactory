package com.webnode.tsy.java.JMF.enums;

/**
 * Enumerace typu dílu
 */
public enum PartType implements SimpleEnum
{
    WASHERS(1, Text.WASHERS),
    SCREWS(2, Text.SCREWS),
    NUT(3, Text.NUT),
    COGWHEEL(4, Text.COGWHEEL);

    /**
     * Vlastní hodnota
     */
    private int value;

    /**
     * Název součástky
     */
    private Text name;


    /**
     * @param value int
     * @param name  String
     */
    PartType(int value, Text name)
    {
        this.value = value;
        this.name = name;
    }


    /**
     * Vrací název součástky
     *
     * @return String
     */
    public String getName()
    {
        return this.name.getText();
    }


    /**
     * Vrací hodnotu typu
     *
     * @return int
     */
    public int getValue()
    {
        return this.value;
    }


    /**
     * Vrátí všechny součástky, které je možné vyrábět v továrně
     *
     * @return int[]
     */
    public static int[] getPossibleOptions()
    {
        int count = PartType.values().length;

        int options[] = new int[count];

        for (PartType pt : PartType.values())
        {
            options[pt.getValue() - 1] = pt.getValue();
        }

        return options;
    }


    /**
     * Vrátí enum podle id
     *
     * @param type int
     * @return PartType
     */
    public static PartType getInstance(int type)
    {
        if (PartType.COGWHEEL.getValue() == type)
        {
            return PartType.COGWHEEL;
        }
        else if (PartType.SCREWS.getValue() == type)
        {
            return PartType.SCREWS;
        }
        else if (PartType.WASHERS.getValue() == type)
        {
            return PartType.WASHERS;
        }
        else if (PartType.NUT.getValue() == type)
        {
            return PartType.NUT;
        }

        return null;
    }
}