package com.webnode.tsy.java.JMF.enums;

/**
 * Stav ve kterém se stroj nachází.
 */
public enum MachineState implements SimpleEnum
{
    /**
     * Stroj je plně funkční a je tedy schopen
     * vykonávat veškeré operace.
     */
    WORKING(1),

    /**
     * Stroj je označený jako nefunkční,
     * stále spotřebovává elektřinu.
     */
    BROKEN(2),

    /**
     * Stroj je vypnutý.
     * Nevykonává žádnou práci
     * a nespotřebovává energii.
     */
    OFF(3);

    /**
     * Vlastní hodnota enumu
     */
    private int value;


    /**
     * Konstruktor pro nastavení hodnoty.
     *
     * @param value int
     */
    MachineState(int value)
    {
        this.value = value;
    }


    /**
     * Vrací numerickou hodnotu enumu
     *
     * @return int
     */
    public int getValue()
    {
        return this.value;
    }

    /**
     * Vrací typ stav stroje podle numerické hodnoty.
     *
     * @param type int
     * @return MachineState|null
     */
    public static MachineState get(int type)
    {
        if (type == MachineState.WORKING.getValue())
        {
            return MachineState.WORKING;
        }
        if (type == MachineState.BROKEN.getValue())
        {
            return MachineState.BROKEN;
        }
        if (type == MachineState.OFF.getValue())
        {
            return MachineState.OFF;
        }

        return null;
    }
}