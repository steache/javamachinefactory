package com.webnode.tsy.java.JMF.enums;


/**
 * Enumerace typu stroje
 */
public enum MachineType implements SimpleEnum
{
    /**
     * První typ stroje, který má typ 1,
     * limit v továrně je neomezený
     * a spotřeba energie je 50/jednotku času.
     */
    A(1, -1, 2, 50),
    B(2, -1, 3, 70),
    C(3, -1, 5, 110),
    D(4, 1, 4, 150),
    E(5, -1, 1, 20);

    /**
     * Vlastní hodnota enumu
     */
    private int value;

    /**
     * Limit počtu tohoto stroje v továrně.
     */
    private int limit = -1;

    /**
     * Spotřeba energie
     */
    private int energyConsumption = -1;


    /**
     * Produkce za hodinu.
     */
    private int production = 0;

    /**
     * Druhý konstruktor, používá se v případě,
     * když je potřeba nastavit limit typu stroje v továrně.
     *
     * @param value      int
     * @param limit      int
     * @param energy     int
     * @param production int
     */
    MachineType(int value, int limit, int energy, int production)
    {
        this.value = value;
        this.limit = limit;
        this.energyConsumption = energy;
        this.production = production;
    }


    /**
     * Vrací maximální počet strojů tohoto typu v továrně.
     *
     * @return int
     */
    public int getLimit()
    {
        return this.limit;
    }


    /**
     * Vrací numerickou hodnotu stroje v továrně.
     *
     * @return int
     */
    public int getValue()
    {
        return this.value;
    }


    /**
     * Vrací všechny možnosti, které lze nastavit u stroje.
     *
     * @return int[]
     */
    public static int[] getPossibleOptions()
    {
        int count = MachineType.values().length;

        int options[] = new int[count];

        for (MachineType mt : MachineType.values())
        {
            options[mt.getValue() - 1] = mt.getValue();
        }

        return options;
    }


    /**
     * Vrací produkci stroje.
     *
     * @return int
     */
    public int getProductionCount()
    {
        return this.production;
    }

    /**
     * Vrací typ stroje podle zadaného id.
     *
     * @param type int
     * @return MachineType
     */
    public static MachineType get(int type)
    {
        if (type == MachineType.A.getValue())
        {
            return MachineType.A;
        }
        if (type == MachineType.B.getValue())
        {
            return MachineType.B;
        }
        if (type == MachineType.C.getValue())
        {
            return MachineType.C;
        }
        if (type == MachineType.D.getValue())
        {
            return MachineType.D;
        }
        if (type == MachineType.E.getValue())
        {
            return MachineType.E;
        }
        return null;
    }
}