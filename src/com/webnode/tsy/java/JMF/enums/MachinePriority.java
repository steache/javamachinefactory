package com.webnode.tsy.java.JMF.enums;

/**
 * Priorita, jakou m� stroj v tov�rn�.
 */
public enum MachinePriority implements SimpleEnum
{
    /**
     * Nejvy��� priorita, tyto stroje se nevyp�naj�,
     * pokud to nen� opravdu nutn�.
     */
    HIGHEST(1),

    /**
     * Stroje s touto prioritou jsou druh� v po�ad�,
     * pokud se jedn� o zapnut�, a p�edposledn�,
     * pokud se jedn� o vypnut�.
     */
    HIGHER(2),

    /**
     * Stroje s pr�m�rnou prioritou, jsou ve st�edu v�eho.
     */
    MEDIUM(3),

    /**
     * Druh� nejni��� priorita, tyto stroje jsou
     * �ast�ji vyp�n�ny, ne� zap�n�ny.
     */
    NORMAL(4),

    /**
     * Nejni��� priorita, tyto stroje jsou vyp�n�ny jako prvn�
     * a zap�n�ny jako posledn�.
     */
    MINOR(5);

    /**
     * V�choz� hodnota, d�le pak aktu�ln� hodnota priority.
     */
    int value = 5;

    /**
     * Konstruktor tohoto enumu, o�ividn� s nimi pracujeme jinak.
     *
     * @param value int
     */
    MachinePriority(int value)
    {
        this.value = value;
    }

    /**
     * Vrac� numerickou hodnotu priority.
     *
     * @return int
     */
    public int getValue()
    {
        return this.value;
    }
}
