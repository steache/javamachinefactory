package com.webnode.tsy.java.JMF.enums;

/**
 * @author Tomáš Sýkora (steache@gmail.com)
 *         <p/>
 *         Většina textů použitá v aplikaci.
 */
public enum Text
{
    CHOOSE("Zadejte prosím jednu z možností."),
    NOT_FOUND("Vámi zvolená možnost neexistuje, zkontrolujte svůj vstup."),
    SPEACIAL_LIMIT("Byl dosažen limit pro tento stroj. Nelze přidat další."),
    CHOOSE_MACHINE_TYPE("Vyberte prosím typ stroje, který chcete přidat."),
    INSERT_INT("Zadejte prosím celé číslo."),
    NOT_VALID_MACHINE("Tento typ stroje neexistuje. Zkuste to prosím znovu."),
    CANCEL_CHOOSE("Zrušit výběr"),
    INSERT_MACHINE_ID("Zadejte prosím ID stroje"),
    NOT_UNIQUE_ID("Takové ID již existuje."),
    NOT_IN_OPTIONS("Vyberete prosím jednu z možností."),
    FIRST_CANCEL_CHOOSE("-1 - zrušit výběr"),
    NOT_VALID_MACHINE_TYPE("Tento typ stroje neexistuje."),
    MACHINE_ID_NOT_EXISTS("Stroj s tímto ID neexistuje."),
    MACHINE_BROKEN_SUCCESSFULY("Stroj byl úspěšně rozbit :D"),
    MACHINE_REPAIRED_SUCCESSFULY("Stroj byl úspěšně opraven"),
    MACHINE_FACTORY_REPORT("Počet strojů v jednotlivých kategoriích: "),
    UNLIMITED("Neomezeno"),
    FACTORY_TOTAL_USAGE("Celková spotřeba továrny je: "),
    COULDNT_LOAD_DATABASE("Nepodařilo se načíst stroje z databáze"),
    COULDNT_SAVE_DATABASE("Nepodařilo se uložit stroje do databáze"),

    // Součástky
    WASHERS("washers"),
    SCREWS("screws"),
    NUT("nut"),
    COGWHEEL("cogwheel");

    /**
     * Samotný text
     */
    private String text;

    /**
     * Privátní konstruktor
     *
     * @param text
     */
    private Text(String text)
    {
        this.text = text;
    }

    public String getText()
    {
        return this.text;
    }
}