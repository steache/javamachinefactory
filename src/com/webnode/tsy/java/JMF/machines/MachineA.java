package com.webnode.tsy.java.JMF.machines;

import com.webnode.tsy.java.JMF.MachineTemplate;
import com.webnode.tsy.java.JMF.enums.MachinePriority;
import com.webnode.tsy.java.JMF.enums.MachineType;
import com.webnode.tsy.java.JMF.enums.PartType;

/**
 *
 */
public class MachineA extends MachineTemplate
{
    /**
     * Konstruktor stroje A
     *
     * @param id int
     */
    public MachineA(int id)
    {
        this.id = id;
        this.energyConsumption = 2;
        this.productionCapacity = 50;

        this.canProduce.add(PartType.WASHERS);

        this.type = MachineType.A;

        // vynulujeme všechny priority
        this.nullAllPriorities();

        // nastavíme priority pro jednotliv součástky
        this.priorities.put(PartType.WASHERS, MachinePriority.HIGHEST);
    }
}