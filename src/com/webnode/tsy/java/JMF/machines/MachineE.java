package com.webnode.tsy.java.JMF.machines;

import com.webnode.tsy.java.JMF.MachineTemplate;
import com.webnode.tsy.java.JMF.enums.MachineType;
import com.webnode.tsy.java.JMF.enums.PartType;

/**
 * @author Tom� S�kora (steache@gmail.com)
 */
public class MachineE extends MachineTemplate
{
    public MachineE(int id)
    {
        this.id = id;

        this.energyConsumption = 1;
        this.productionCapacity = 20;

        this.canProduce.add(PartType.WASHERS);
        this.canProduce.add(PartType.COGWHEEL);

        this.type = MachineType.E;
    }
}