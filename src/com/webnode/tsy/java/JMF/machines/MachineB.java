package com.webnode.tsy.java.JMF.machines;

import com.webnode.tsy.java.JMF.MachineTemplate;
import com.webnode.tsy.java.JMF.enums.MachinePriority;
import com.webnode.tsy.java.JMF.enums.MachineType;
import com.webnode.tsy.java.JMF.enums.PartType;

/**
 *
 */
public class MachineB extends MachineTemplate
{
    public MachineB(int id)
    {
        this.id = id;
        this.energyConsumption = 3;
        this.productionCapacity = 70;

        this.canProduce.add(PartType.WASHERS);
        this.canProduce.add(PartType.SCREWS);

        this.type = MachineType.B;

        this.nullAllPriorities();

        // nastavíme priority pro jednotliv součástky
        this.priorities.put(PartType.SCREWS, MachinePriority.HIGHEST);
        this.priorities.put(PartType.WASHERS, MachinePriority.HIGHER);
    }
}