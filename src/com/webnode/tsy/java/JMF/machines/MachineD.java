package com.webnode.tsy.java.JMF.machines;

import com.webnode.tsy.java.JMF.MachineTemplate;
import com.webnode.tsy.java.JMF.enums.MachinePriority;
import com.webnode.tsy.java.JMF.enums.MachineType;
import com.webnode.tsy.java.JMF.enums.PartType;

/**
 *
 */
public class MachineD extends MachineTemplate
{
    /**
     * @param id int
     */
    public MachineD(int id)
    {
        this.id = id;

        this.energyConsumption = 4;
        this.productionCapacity = 150;

        this.canProduce.add(PartType.WASHERS);

        this.type = MachineType.D;

        this.nullAllPriorities();

        // nastavíme priority pro jednotliv součástky
        this.priorities.put(PartType.WASHERS, MachinePriority.HIGHEST);
    }
}