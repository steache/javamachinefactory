package com.webnode.tsy.java.JMF.machines;

import com.webnode.tsy.java.JMF.MachineTemplate;
import com.webnode.tsy.java.JMF.enums.MachinePriority;
import com.webnode.tsy.java.JMF.enums.MachineType;
import com.webnode.tsy.java.JMF.enums.PartType;

public class MachineC extends MachineTemplate
{
    public MachineC(int id)
    {
        this.id = id;

        this.energyConsumption = 5;
        this.productionCapacity = 110;

        this.canProduce.add(PartType.WASHERS);
        this.canProduce.add(PartType.SCREWS);
        this.canProduce.add(PartType.NUT);

        this.type = MachineType.C;

        this.nullAllPriorities();

        // nastavíme priority pro jednotliv součástky
        this.priorities.put(PartType.SCREWS, MachinePriority.HIGHER);
        this.priorities.put(PartType.WASHERS, MachinePriority.MEDIUM);
        this.priorities.put(PartType.NUT, MachinePriority.HIGHEST);
    }
}