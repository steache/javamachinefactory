package com.webnode.tsy.java.JMF;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.EnumMap;

import com.webnode.tsy.java.JMF.enums.*;
import com.webnode.tsy.java.JMF.exceptions.*;

/**
 * @author Tomáš Sýkora (steache@gmail.com)
 */
public class Factory extends SimpleThings
{
    /**
     *
     */
    private String fileName;

    /**
     * Pole strojů v továrně, klíčem je typ stroje.
     * Hodnota je array list strojů.
     * ( Jednodušší práce add/remove, list, možná je trochu náročnější... )
     */
    private EnumMap<MachineType, ArrayList<MachineTemplate>> machines;

    /**
     * Aktuální produkce na díl
     */
    private EnumMap<PartType, Integer> parts;


    /**
     * @return
     */
    public EnumMap<PartType, Integer> getParts()
    {
        return this.parts;
    }

    /**
     * Konstruktor
     * - inicializuje pole enummap strojů
     * - incializuje počty strojů
     */
    public Factory()
    {
        this.machines = new EnumMap<MachineType, ArrayList<MachineTemplate>>(MachineType.class);
        this.parts = new EnumMap<PartType, Integer>(PartType.class);

        for (MachineType mt : MachineType.values())
        {
            this.machines.put(mt, new ArrayList<MachineTemplate>());

        }

        for (PartType pt : PartType.values())
        {
            this.parts.put(pt, 0);
        }

        this.fileName = "db.txt";
    }


    /**
     * Přidá stroj do továrny
     *
     * @throws SpecialMachineLimitReached
     * @throws NotUniqueMachineId
     */
    public void addMachine(MachineTemplate m) throws SpecialMachineLimitReached, NotUniqueMachineId
    {
        MachineType t = m.getType();

		/*
         * Kontrolujeme, zda je id unikátní
		 */
        for (MachineType mtype : MachineType.values())
        {
            for (MachineTemplate mt : this.machines.get(mtype))
            {
                if (mt.getId() == m.getId())
                {
                    throw new NotUniqueMachineId();
                }
            }
        }

		/*
         * Kontrolujeme, zda není bychom přidáním nepřekročili limit,
		 * který je nastavneý pro určitý typ stroje.
		 */
        if (t.getLimit() != -1 && (this.machines.get(t).size() + 1) > t.getLimit())
        {
            throw new SpecialMachineLimitReached();
        }

		/*
         * Pokud o nepřidáváme přes guide, ale import,
		 * tak se může stát, že má již nastavené production, tak jej přičteme
		 */
        this.increasePartProduction(m.getProducingPartType(), m.getActualProduction());

		/*
		 * Přidáme stroj do továrny
		 */
        this.machines.get(t).add(m);
    }


    /**
     * Metoda pro odstranění stroje
     * - pokud najde stroj, tak ho odstraní
     * - v opačném případě vyhodí chybu, když nenalezl stroj
     *
     * @param id
     * @throws FactoryException
     */
    public void removeMachine(int id) throws FactoryException
    {
        for (MachineType mtype : MachineType.values())
        {
            for (MachineTemplate mt : this.machines.get(mtype))
            {
                if (mt.getId() == id)
                {
                    this.machines.get(mtype).remove(mt);
                    return;
                }
            }
        }

        throw new FactoryException();
    }


    /**
     * Je jeden z typů strojů?
     *
     * @param index
     * @return
     */
    public boolean isInRangeOfMachines(int index)
    {
        for (MachineType mt : MachineType.values())
        {
            if (mt.getValue() == index)
            {
                return true;
            }
        }

        return false;
    }


    /**
     * Metoda pro nalezení stroje podle id
     *
     * @param id
     * @return
     * @throws MachineNotExists
     */
    public MachineTemplate getMachine(int id) throws MachineNotExists
    {
        for (MachineType mtype : MachineType.values())
        {
            for (MachineTemplate mt : this.machines.get(mtype))
            {
                if (mt.getId() == id)
                {
                    return mt;
                }
            }
        }

        throw new MachineNotExists();
    }


    /**
     * Nastavuje stav stroje na rozbitý
     *
     * @param id
     * @throws MachineNotExists
     */
    public void breakMachine(int id) throws MachineNotExists
    {
        this.getMachine(id).setState(MachineState.BROKEN);
    }


    /**
     * Nastavuje typ stroje na funkční
     *
     * @param id
     * @throws MachineNotExists
     */
    public void repairMachine(int id) throws MachineNotExists
    {
        this.getMachine(id).setState(MachineState.OFF);
    }


    /**
     * Vrací pole strojů
     *
     * @return
     */
    public EnumMap<MachineType, ArrayList<MachineTemplate>> getMachines()
    {
        return this.machines;
    }


    /**
     * Jednoduché vyhledání, zda je možné přehodit výrobu součástky na jiný stroj
     *
     * @param id
     * @return
     * @throws MachineNotExists
     */
    public boolean optimizeMachine(int id, PartType pt, int count) throws MachineNotExists
    {
        MachineTemplate machine = this.getMachine(id);

        for (MachineTemplate mt : this.machines.get(machine.getType()))
        {
            if (mt.getState() == MachineState.OFF && mt.getType().getProductionCount() >= count)
            {
                mt.setState(MachineState.WORKING);
                mt.setProducingPart(pt);
                return true;
            }
        }

        return false;
    }


    /**
     * Zjišťuje, zda může zvýšit produkci pouhým zapnutím.
     *
     * @param pt
     * @param count
     * @return
     */
    public boolean canIncreaseBySwitchingOn(PartType pt, int count)
    {
        int canHandle = 0;

        for (MachineType mt : MachineType.values())
        {
            for (MachineTemplate m : this.machines.get(mt))
            {
                if (m.getState() == MachineState.OFF && m.canProduce(pt))
                {
                    canHandle += m.getType().getProductionCount();
                }
            }
        }

        return (canHandle >= count);
    }


    /**
     * Zaznamená zvýšení počtu výroby dílu
     *
     * @param pt    PartType
     * @param count int
     */
    public void increasePartProduction(PartType pt, int count)
    {
        this.parts.put(pt, this.parts.get(pt) + count);
    }
	
	
	/*private void decreasePartProduction(PartType pt, int count)
	{
		this.parts.put(pt, this.parts.get(pt) - count);
	}*/


    /**
     * Zapne všechny stroje, které umí vyrábět daný typ dílu,
     * dokud nedosáhne požadované výroby, nebo nezapne všechny stroje.
     *
     * @param pt    PartType
     * @param count int
     */
    public void startNonWorkingMachinesForType(PartType pt, int count)
    {
        int ensured = 0;

        for (MachineType mt : MachineType.values())
        {
            for (MachineTemplate m : this.machines.get(mt))
            {
                if (m.getState() == MachineState.OFF && m.canProduce(pt) && ensured < count)
                {
                    ensured += m.getType().getProductionCount();
                    m.setState(MachineState.WORKING);
                    m.setActualProduction(m.getType().getProductionCount());
                }
            }
        }
    }

    /**
     * Vypneš všechny zapnuté stroje
     * - neodečítá z produkce
     */
    public void shutOffAllWorkingMachines()
    {
        for (MachineType mt : MachineType.values())
        {
            for (MachineTemplate m : this.machines.get(mt))
            {
                if (m.isWorking())
                {
                    m.setState(MachineState.OFF);
                }
            }
        }
    }

    public void saveDatabase() throws IOException
    {
        File db = new File(this.fileName);

        FileWriter writer;

        writer = new FileWriter(db);

        for (MachineType mt : MachineType.values())
        {
            for (MachineTemplate m : this.getMachines().get(mt))
            {
                writer.write(
                        m.getId() + ";" +
                                m.getType().getValue() + ";" +
                                m.getActualProduction() + ";" +
                                m.getState().getValue() + ";" +
                                m.getProducingPartType().getValue() + "\n"
                );
            }
        }

        writer.close();
    }

    /**
     * Načte databázi ze souboru
     *
     * @throws IOException
     * @throws SpecialMachineLimitReached
     * @throws NotUniqueMachineId
     * @throws FactoryException
     */
    public void loadDatabase() throws IOException, SpecialMachineLimitReached, NotUniqueMachineId, FactoryException
    {
        File db = new File(this.fileName);

        FileReader reader;

        String row;

        BufferedReader in;

        reader = new FileReader(db);

        in = new BufferedReader(reader);

        while ((row = in.readLine()) != null)
        {
            String[] columns = row.split(";");

            // 0 -> tady bude id

            // asi by to chtělo ohlídat... to vyřešíme okamžitě
            int id = Integer.parseInt(columns[0]);

            //type
            int type = Integer.parseInt(columns[1]);

            // production
            int production = Integer.parseInt(columns[2]);

            // state
            int state = Integer.parseInt(columns[3]);

            // actual part
            int part = Integer.parseInt(columns[4]);

            // vytvoříme si objekt
            MachineTemplate machine = MachineFactory.getMachine(type, id);

            //
            machine.setActualProduction(production);

            //
            machine.setState(MachineState.get(state));

            //
            machine.setProducingPart(PartType.getInstance(part));

            //Zkusíme ho přidat, pokud to nedopadne, tak skončíme... máme chybu v datech a nebudeme se ji pokoušet opravovat
            this.addMachine(machine);
        }

        reader.close();
    }
}
