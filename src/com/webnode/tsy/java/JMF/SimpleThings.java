package com.webnode.tsy.java.JMF;

import java.util.InputMismatchException;
import java.util.Scanner;

import com.webnode.tsy.java.JMF.enums.Text;

/**
 * Všespásná třída pro ošetřené načítání vstupů :)
 */
abstract class SimpleThings
{
    /**
     * @param text String
     */
    protected void p(String text)
    {
        System.out.println(text);
    }


    /**
     * @param text Text
     */
    protected void p(Text text)
    {
        System.out.println(text.getText());
    }


    protected Scanner sc = new Scanner(System.in);


    /**
     * @return int
     */
    protected int getInt()
    {
        int value = 0;

        try
        {
            value = this.sc.nextInt();
        }
        catch (InputMismatchException e)
        {
            this.p(Text.INSERT_INT);
            this.sc.next();
            value = this.getInt();
        }

        return value;
    }


    /**
     * @return String
     */
    protected String getString()
    {
        String value = "";

        try
        {
            value = this.sc.next();
        }
        catch (InputMismatchException e)
        {
            this.p(Text.INSERT_INT);
            this.sc.next();
            value = this.getString();
        }

        return value;
    }


    /**
     * @return double
     */
    protected double getDouble()
    {
        double value = 0;

        try
        {
            value = this.sc.nextDouble();
        }
        catch (InputMismatchException e)
        {
            this.p(Text.INSERT_INT);
            this.sc.next();
            value = this.getDouble();
        }

        return value;
    }


    /**
     * Metoda vyu��v� metody getInt pro na�ten� vybran� volby.
     * Pokud je vstup jedna ze zadan�ch mo�nost�, tak ji vr�t�
     * v opa�n�m p��pad� nech� u�ivatele znovu na��st.
     *
     * @param options
     * @return
     */
    protected int getIntFromOptions(int[] options, String question)
    {
        boolean valid  = false;
        int     option = 0;
        //vypsat mo�nosti

        p(question);

        outerloop:
        while (valid != true || option != -1)
        {
            option = this.getInt();

            for (int o : options)
            {
                if (o == option)
                {
                    valid = true;
                    break outerloop;
                }
            }

            if (!valid)
            {
                p(Text.NOT_IN_OPTIONS);
            }
        }

        return option;
    }
}