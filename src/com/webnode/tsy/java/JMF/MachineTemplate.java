package com.webnode.tsy.java.JMF;

import java.util.ArrayList;
import java.util.EnumMap;

import com.webnode.tsy.java.JMF.enums.*;

/**
 * Předek všech strojů v továrně.
 */
public abstract class MachineTemplate extends SimpleThings
{
    /**
     * Identifikátor stroje v továrně. Je jedinečný.
     */
    protected int id;

    /**
     * Typ stroje, enum MachineType
     */
    protected MachineType type;

    /**
     * Stav, ve kterém se stroj aktuálně nachází.
     */
    protected MachineState state = MachineState.OFF;

    /**
     * Spotřeba energie na součástku.
     */
    protected int energyConsumption;

    /**
     * Maximální produkce za hodinu.
     */
    protected int productionCapacity;

    /**
     * Aktuálně vyráběná součástka.
     */
    protected PartType producing = PartType.WASHERS;

    /**
     * Aktuální počet vyráběných dílů za hodinu.
     */
    protected int actualProduction = 0;

    /**
     * Priority výroby pro jednotlivé součástky
     * - nejvyšší prioritu má součástka, která se vyrábí speciální na tomto stroji
     * - v případě WASHERS, které vyrábí všichni se nastavuje priorita podle produkce
     * - nejvyšší prioritu mají stroje s nejvyšší možnou výrobou
     */
    protected EnumMap<PartType, MachinePriority> priorities = new EnumMap<PartType, MachinePriority>(PartType.class);

    /**
     * Pole součástek, které může stroj vyrábět.
     */
    protected ArrayList<PartType> canProduce = new ArrayList<PartType>();


    /**
     * Vrací typ stroje
     *
     * @return MachineType
     */
    public MachineType getType()
    {
        return this.type;
    }


    /**
     * Nastavuje id stroje
     *
     * @param id int
     */
    protected void setId(int id)
    {
        this.id = id;
    }


    /**
     * @return int
     */
    protected int getId()
    {
        return this.id;
    }


    /**
     * Nastaví stav stroje na zadanou hodnotu
     *
     * @param ms
     */
    protected void setState(MachineState ms)
    {
        this.state = ms;
    }


    /**
     * Vrací aktuální stav stroje
     *
     * @return MachineState
     */
    protected MachineState getState()
    {
        return this.state;
    }


    /**
     * Vrací maximální spotřebu stroje
     *
     * @return int
     */
    protected int getEnergyConsumption()
    {
        return this.energyConsumption;
    }


    /**
     * Vrací typ aktuálně vyráběné součástky
     *
     * @return PartType
     */
    protected PartType getProducingPartType()
    {
        return this.producing;
    }


    /**
     * Nastavuje typ aktuálně vyráběné součástky
     *
     * @param pt
     */
    protected void setProducingPart(PartType pt)
    {
        this.producing = pt;
    }


    /**
     * Zjišťuje, zda může stroj vyrábět tuto součástku.
     *
     * @param pt PartType
     * @return boolean
     */
    protected boolean canProduce(PartType pt)
    {
        for (PartType ptx : this.canProduce)
        {
            if (ptx.getValue() == pt.getValue())
            {
                return true;
            }
        }

        return false;
    }


    /**
     * Zjišťuje, zda je stroj mimo nepracovní stavy.
     * ( teď je to pouze !BROKEN && !OFF --> == WORKING)
     *
     * @return boolean
     */
    protected boolean isWorking()
    {
        return (this.state == MachineState.WORKING);
    }


    /**
     * Nastavuje typ součástku na null u všech priorit
     * ( předchází NullPointerException při dotazech )
     */
    protected void nullAllPriorities()
    {
        for (PartType pt : PartType.values())
        {
            this.priorities.put(pt, null);
        }
    }


    /**
     * Vrátí počet aktuálně vyráběných dílů za hodinu
     *
     * @return int
     */
    protected int getActualProduction()
    {
        return this.actualProduction;
    }


    /**
     * Nastavuje počet vyráběných dílů za hodinu
     *
     * @param count int
     */
    protected void setActualProduction(int count)
    {
        this.actualProduction = count;
    }
}