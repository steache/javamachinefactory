package com.webnode.tsy.java.JMF.exceptions;

/**
 * Vyj�mka, kter� vznik�, pokud do tov�rny
 * p�id�v�m v�c stroj� se stejn�m identifik�torem.
 */
public class NotUniqueMachineId extends Exception
{
}
