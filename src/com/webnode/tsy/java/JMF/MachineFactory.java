package com.webnode.tsy.java.JMF;

import com.webnode.tsy.java.JMF.enums.*;
import com.webnode.tsy.java.JMF.exceptions.*;
import com.webnode.tsy.java.JMF.machines.*;

/**
 * Továrna na tvorbu instancí stroje daného typu
 */
public class MachineFactory
{
    /**
     * Vrací instanci stroje podle typu a nastaví mu id
     *
     * @param type int
     * @param id   int
     * @return MachineTemplate
     * @throws FactoryException
     */
    public static MachineTemplate getMachine(int type, int id) throws FactoryException
    {
        if (MachineType.A.getValue() == type)
        {
            return new MachineA(id);
        }

        if (MachineType.B.getValue() == type)
        {
            return new MachineB(id);
        }

        if (MachineType.C.getValue() == type)
        {
            return new MachineC(id);
        }

        if (MachineType.D.getValue() == type)
        {
            return new MachineD(id);
        }

        if (MachineType.E.getValue() == type)
        {
            return new MachineE(id);
        }

        throw new FactoryException();
    }
}
