package com.webnode.tsy.java.JMF;

import java.io.IOException;
import java.util.EnumMap;

import com.webnode.tsy.java.JMF.enums.*;
import com.webnode.tsy.java.JMF.exceptions.*;

/**
 * @author tsy <tomas.sykora@webnode.com>
 */
public class Launcher extends SimpleThings
{
    /**
     * Možnost zvolená v menu.
     */
    private int option = 0;

    /**
     * Instance továrny.
     */
    private Factory factory;

    /**
     * @param args String[]
     */
    public static void main(String[] args)
    {
        Launcher l = new Launcher();

        l.run();
    }

    /**
     *
     */
    public Launcher()
    {
        this.factory = new Factory();
    }

    /**
     * Hlavn� cyklus programu
     */
    public void run()
    {
        try
        {
            this.factory.loadDatabase();
        }
        catch (IOException e)
        {
            p(Text.COULDNT_LOAD_DATABASE);
        }
        catch (FactoryException e)
        {
            p(Text.NOT_VALID_MACHINE_TYPE);
        }
        catch (SpecialMachineLimitReached e)
        {
            p(Text.SPEACIAL_LIMIT);
        }
        catch (NotUniqueMachineId e)
        {
            p(Text.NOT_UNIQUE_ID);
        }

        while (this.option != -1)
        {
            this.printMenu();

            this.option = this.getInt();

            this.options();
        }

        try
        {
            this.factory.saveDatabase();
        }
        catch (IOException e)
        {
            p(Text.COULDNT_SAVE_DATABASE);
        }
    }

    /**
     * Vol� se po volb� v menu.
     */
    public void options()
    {
        switch (this.option)
        {
            case -1:
                break;
            case 0:
                this.addNewMachineGuide();
                break;
            case 1:
                this.removeMachineGuide();
                break;
            case 2:
                this.machineFailureGuide();
                break;
            case 3:
                this.machineRepairGuide();
                break;
            case 4:
                this.factoryMachineReport();
                break;
            case 5:
                this.factoryEnergyConsumptionReport();
                break;
            case 6:
                this.factoryProductionReport();
                break;
            case 7:
                this.increaseProductionGuide();
                break;
            default:
                p(Text.NOT_FOUND);
                break;
        }
    }


    private void increaseProductionGuide()
    {
        p("Vítejte v menu pro zvýšení kapacity výroby :)");

        for (PartType pt : PartType.values())
        {
            p(pt.getValue() + " - " + pt.getName());
        }

        int choose = this.getIntFromOptions(PartType.getPossibleOptions(), "Který chcete vyrábět díl");

        p("V jaké kapacitě?");
        int capacity = this.getInt();

        PartType pt = PartType.getInstance(choose);

        if (this.factory.canIncreaseBySwitchingOn(pt, capacity))
        {
            p("Můžeme zvýšit produkci pouhým zapnutím vypnutých strojů a dosáhneme požadované produkce.");

            p("Máme to udělat ?");

            int[] options = {1, 2};

            int answer = this.getIntFromOptions(options, "1 pro ano, 2 pro ne");

            if (answer == 1)
            {
                this.factory.startNonWorkingMachinesForType(pt, capacity);
                this.factory.increasePartProduction(pt, capacity);
            }

            return;
        }

        // optimalizace

        EnumMap<PartType, Integer> handled_s = new EnumMap<PartType, Integer>(PartType.class);

        for (PartType py : PartType.values())
        {
            handled_s.put(py, 0);
        }

        p("vypnul jsem všechny stroje, které byly working ... ");
        this.factory.shutOffAllWorkingMachines();

        this.factory.getParts().put(pt, this.factory.getParts().get(pt) + capacity);

        // projdu všechny typy součástek
        for (MachinePriority mp : MachinePriority.values())
        {
            //if(this.factory.getParts().get(pt1) == 0) continue;

            p("řeším prioritu:" + mp.getValue());

            // vynuluji si počítadlo, kolik jsem zvládnul zaobstarat :D


            // projdu priority a zkusím nejdřív nastavit stroje,
            // které mají na tento typ dílu nejvyšší prioritu
            for (PartType pt1 : PartType.values())
            {
                int handled = 0;

                p("\t pt: " + pt1.getName());

                // projdu všechny typy strojů
                for (MachineType mt : MachineType.values())
                {
                    p("\t typ: " + mt.getValue());

                    // všechny stroje
                    for (MachineTemplate m : this.factory.getMachines().get(mt))
                    {
                        p("\t \t id stroje: " + m.getId());

                        p("\t \t \t before-handled : " + handled);

                        // pokud jsem na stroji, který má tuto prioritu
                        // tak jej přičtu

                        //p("stroj:" + m.priorities.get(pt1).getValue() + "HANDLED < " + handled + " < " + (this.factory.getParts().get(pt1) + capacity) );

                        p("\t \t \t mam kapacitu: " + m.getType().getProductionCount());

                        try
                        {
                            p(m.priorities.get(pt1).getValue() + " == ");
                        }
                        catch (NullPointerException e)
                        {
                            p("prvni");
                        }

                        try
                        {
                            p(mp.getValue() + " ");
                        }
                        catch (NullPointerException e)
                        {
                            p("druhy");
                        }


                        p("\t \t \t pt1 " + "|" + handled + "<" + (this.factory.getParts().get(pt1) + capacity) + "w: " + !m.isWorking());

                        if (m.priorities.get(pt1) == mp && handled < (this.factory.getParts().get(pt1) + capacity) && !m.isWorking())
                        {
                            m.setState(MachineState.WORKING);
                            handled += m.getType().getProductionCount();
                            m.setProducingPart(pt1);
                            handled_s.put(pt1, handled_s.get(pt1) + m.getType().getProductionCount());
                        }

                        p("\t \t \t after-handled : " + handled);
                    }
                }

                // pokud jsem prošel jednu prioritu, tak se podívám, jestli už nemám dost ...
                if (this.factory.getParts().get(pt1) >= handled)
                {
                    break;
                }
            }

			/*if((handled+capacity) <= this.factory.getParts().get(pt))
            {
				this.factory.getParts().put(pt1, (handled+capacity) );
				p("zvládli jsme najít dost strojů, které jsme zapnuli a našli jsme dohromady" + handled);
			}
			else
			{
				if(handled == 0)
				{
					this.factory.getParts().put(pt1, 0);
				}
				else
				{
					this.factory.getParts().put(pt1, handled);
				}
				
				handled = 0;
				
				p("nezvládli jsme to pouze: " + handled);
			}*/
        }

        for (PartType py : PartType.values())
        {
            p("Sehnal jsem: " + handled_s.get(py) + "/" + this.factory.getParts().get(py) + "u " + py.getName());
        }


        //nabídka přidání nového stroje

    }

    /**
     * Guide na přidávání nového stroje
     * - nejdřív vybere uživatel typ stroje
     * - kontroluje se v rozsahu typů strojů
     * - potom zadá id
     * - kontrola unikátnosti ID :)
     */
    private void addNewMachineGuide()
    {
        int choose;
        for (MachineType mt : MachineType.values())
        {
            p(mt.getValue() + " - " + mt.toString());
        }

        choose = this.getIntFromOptions(MachineType.getPossibleOptions(), Text.CHOOSE_MACHINE_TYPE.getText());

        boolean ok = false;
        while (ok != true)
        {
            p(Text.INSERT_MACHINE_ID);

            int id = this.getInt();

            try
            {
                MachineTemplate mt = MachineFactory.getMachine(choose, id);

                this.factory.addMachine(mt);

                ok = true;
            }
            catch (FactoryException e)
            {
                p(Text.NOT_VALID_MACHINE_TYPE);
            }
            catch (SpecialMachineLimitReached e)
            {
                p(Text.SPEACIAL_LIMIT);
                break;
            }
            catch (NotUniqueMachineId e)
            {
                p(Text.NOT_UNIQUE_ID);
            }
        }
    }


    /**
     * Guide na vymazání stroje z továrny.
     * - pokusí se načíst id stroje
     * - pokusí se jej odstranit,
     * pokud neuspěje, tak ukončí guide
     */
    private void removeMachineGuide()
    {
        try
        {
            p(Text.INSERT_MACHINE_ID);

            int id = getInt();

            this.factory.removeMachine(id);
        }
        catch (FactoryException e)
        {
            p(Text.MACHINE_ID_NOT_EXISTS);
        }
    }


    /**
     * Guide pro rozbití stroje v továrně :D
     */
    private void machineFailureGuide()
    {
        p(Text.INSERT_MACHINE_ID);
        int id = this.getInt();

        try
        {
            this.factory.breakMachine(id);
            p(Text.MACHINE_BROKEN_SUCCESSFULY);
        }
        catch (MachineNotExists e)
        {
            p(Text.MACHINE_ID_NOT_EXISTS);
            return;
        }

        // pokusíme se nahradit výrobu

        try
        {
            MachineTemplate mt = this.factory.getMachine(id);

            boolean success = this.factory.optimizeMachine(id, mt.getProducingPartType(), mt.getType().getProductionCount());

            if (success)
            {
                p("stroj byl nahrazen jiným, který zařídí všechnu práci ...");
            }
            else
            {
                p("nebylo možné nahradit práci tohoto stroje jiným strojem :/");
            }
        }
        catch (MachineNotExists e)
        {
        }


    }


    /**
     * Guide pro opravení stroje v továrně
     */
    private void machineRepairGuide()
    {
        p(Text.INSERT_MACHINE_ID);
        int id = this.getInt();

        try
        {
            this.factory.repairMachine(id);
            p(Text.MACHINE_REPAIRED_SUCCESSFULY);
        }
        catch (MachineNotExists e)
        {
            p(Text.MACHINE_ID_NOT_EXISTS);
        }
    }


    /**
     * Výpis strojů v jednotlivých kategoriích.
     */
    private void factoryMachineReport()
    {
        p(" ");
        p(Text.MACHINE_FACTORY_REPORT);

        for (MachineType mtype : MachineType.values())
        {
            String text = "";

            text += this.factory.getMachines().get(mtype).size();

            text += "/";

            text += (mtype.getLimit() == -1)
                    ? Text.UNLIMITED.getText()
                    : mtype.getLimit();

            p(" - " + text);
        }
        p(" ");
    }


    /**
     * Výpis celkové spotřeby strojů
     */
    private void factoryEnergyConsumptionReport()
    {
        int total = 0;

        for (MachineType mtype : MachineType.values())
        {
            for (MachineTemplate mt : this.factory.getMachines().get(mtype))
            {
                if (mt.getState() == MachineState.WORKING)
                {
                    total += mt.getEnergyConsumption();
                }
            }
        }

        p(" ");
        p(" " + Text.FACTORY_TOTAL_USAGE.getText() + total + "W/h");
        p(" ");
    }


    private void factoryProductionReport()
    {
        p("Výpis aktuálně vyráběných součástek a počtů: ");

        p(" ");

        for (PartType pt : PartType.values())
        {
            p(" - " + pt.getName() + ": " + this.factory.getParts().get(pt));
        }

        p(" ");
    }


    /**
     * Výpis menu v hlavním cyklu
     */
    private void printMenu()
    {
        p(Text.CHOOSE);

        String[] menu = new String[]{
                "-1 | Konec programu",
                "0-3| stroj",
                "   | 0 - přidat, 1 - odstranit, 2-porucha, 3-oprava",
                " 4 | Výpis počtů strojů v kategoriích",
                " 5 | Výpis celkové spotřeby energie",
                " 6 | Výpis aktuálně vyráběných dílů a počet",
                " 7 | Zvýšit produkci součástky o ",
        };

        for (String s : menu)
        {
            p(s);
        }
    }
}